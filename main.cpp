#include <iostream>
#include <ctime>

using namespace std;

class CoffeeMachine {
public:
    bool Start(int LevelWater) {
        if(CheckWater(LevelWater)) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
            return true;
        }
        else {
            cout << "Sorry, low level of water! Add more water." << endl;
            return false;
        }
    }
    virtual void Temperature() const {
        cout << "Temperature of Your drink 100 degrees" << endl;
    }
private:
    bool CheckWater(int LevelWater) {
        if(LevelWater > 100) {
            return true;
        }
        else {
            return false;
        }
    }
};

class Tea : public CoffeeMachine {
public:
    bool Start(int LevelWater, int sugar) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater) && sugar >= 0) {
            cout << "Price of Your drink 1$" << endl;
			cout << "Number of sugar blocks is: " << sugar << endl;
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 80 degrees" << endl;
    }
};

class Coffee : public CoffeeMachine {
public:
    bool Start(int LevelWater, , int sugar) {
        
        if(cm.Start(LevelWater)) {
            cout << "Price of Your drink 2$" << endl;
			cout << "Number of sugar blocks is: " << sugar << endl;
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 70 degrees" << endl;
    }
};

class Latte : public Coffee {
public:
    bool Start(int LevelWater, int sugar) {
        Coffee cm;
        if(Coffee.Start(LevelWater, int sugar)) {
            cout << "Price of milk 0.5$" << endl;
            return true;
        }
    void Temperature() const {
        Coffee cm;
        cm.Temperature();
    }
};

void Temperature(const CoffeeMachine &obj) {
    obj.Temperature();
}

int main() {
    srand(time(NULL));
    int level = rand() % 1000;
    cout << "Level of water: " << level << endl;
    int option = 0;
    Tea t;
    Coffee c;
    Latte l;
    cout << "Enter 1 - for coffee, 2 - for tea, 3 - for Latte: " << endl;
    cin >> option;
    if(option == 1) {
        if(c.Start(level)) {
            Temperature(c);
        }
    }
    else if(option == 2) {
        if(t.Start(level)) {
            Temperature(t);
        }
    }
	else if(option == 3) {
       if(l.Start(level)) {
            Temperature(l);
        }
    }
    return 0;
}
